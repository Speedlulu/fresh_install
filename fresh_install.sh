#! /bin/bash

packages='git ssh python3 curl wget pass python3-ipdb ipython3 zsh terminator man tig'
plugins='sudo git pass'
theme='powerlevel10k\/powerlevel10k'
git_username='Lucas Ficheux'
git_email='lucas.ficheux@gmail.com'
# Seperate hosts by spaces, format : ip_hostname
hosts='217.160.66.50_edoras'

if [[ "$*" == '--spotify' ]]
then
    spotify_repo='deb http://repository.spotify.com stable non-free'
    sudo -- sh -c "echo '$spotify_repo' >> /etc/apt/sources.list.d/spotify.list"
    packages="$packages spotify"
fi

if [[ $XDG_CURRENT_DESKTOP == 'GNOME' ]]
then
    packages="$packages gnome-tweaks"
fi

sudo apt update -y
sudo apt upgrade -y
sudo apt install -y $packages

# make ping usable by non root
sudo setcap 'cap_net_raw+ep' $(which ping)

sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" -s --batch

# p10k stuff
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k

wget https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf
wget https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Italic.ttf
wget https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold.ttf
wget https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold%20Italic.ttf
chmod 644 *.ttf
sudo mv *.ttf /usr/local/share/fonts/
fc-cache

# add hosts
for host in $hosts
do
    sed -i "s/\# The following lines/$(echo $host | sed 's/_/\t/g')\n\# The following lines/g" /etc/hosts
done
# insert final \n if hosts
[[ $hosts ]] && sed -i "s/\# The following lines/\n\# The following lines/g" /etc/hosts

# add aliases, plugins, theme and ZLE

sed -i "s/plugins=([^()]*)/plugins=($plugins)/g" $HOME/.zshrc
sed -i "s/ZSH_THEME=\"[^\"]*\"/ZSH_THEME=\"$theme\"/g" $HOME/.zshrc

echo "ZLE_RPROMPT_INDENT=0\n" >> $HOME/.zshrc

echo "export EDITOR='$(which vim)'" >> $HOME/.zshrc
echo "export VISUAL='$(which vim)\n'" >> $HOME/.zshrc

echo "alias zshrc='vi ~/.zshrc; source ~/.zshrc'" >> $HOME/.zshrc
echo "alias ipython='ipython3'\n" >> $HOME/.zshrc

echo "mkcd() { mkdir -p -- $1 && cd -P -- $1 }"

# git config

git config --global pull.rebase = true
git config --global user.name="$git_username"
git config --global user.email="$git_email"

# interactive part

chsh -s $(which zsh)

terminator & disown

echo 'Close this terminal'

exit 0
